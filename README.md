# 嵌入式好用工具—软件篇

## 一、串口相关

### （1）XCOM V2.0（经典）

![](image/3.png)

* 功能便捷
* 体积小
* 传播广

### （2）山外串口调试助手

![](image/2.png)

![](image/1.png)

* 十六进制收发方便（可以直接复制十六进制进去）
* 可以显示波形（多通道*虚拟示波器）
* GSM模块调试功能
* GPS模块调试功能
* TCP调试功能

### （3）Modbus调试精灵

![](image/4.png)

* Modbus从设备数据收发
* 体积小、功能便捷

### （4）Modbus调试工具

![](image/5.png)

* Modbus全功能调试（线圈读写、寄存器读写）
* 模拟从设备
* 模拟主设备

### （5）虚拟串口

![](image/12.png)

* 实现串口绑定
* 方便调试协议测试（蓝牙、zigbee、433M）
* 模拟配合上位机（比如假定了协议，方便模拟从设备）

## 二、下载相关

### （1）普中下载

![](image/6.png)

* 功能简单
* 下载芯片局限性
* 大部分人的下载启蒙软件

### （2）STC-ISP

![](image/7.png)

![](image/8.png)

* STC官方烧录调试软件
* 全部STC芯片下载
* 串口助手
* 范例程序（全部STC）
* 波特率计算器
* 定时器计算器
* 软件延时计算器

### （3）MCUISP

![](image/9.png)

* 常见cortex-M内核串口下载
* 功能单一（适用于无SWD和JTAG引脚接出时使用）

### （4）NodeMCU PyFlasher（esp8266下载器/配合arduino）

![](image/10.png)

* 功能单一（下载ESP8266）
* 操作简单（同乐鑫的烧录软件比起）

## 三、开发相关

### （1）BinaryViewer(HEX文件查看器)

![](image/11.png)

* 分析数据格式
* 判断数据结构

### （2）PCtoLCD2002（取字模软件）

![](image/13.png)

* 适用于无字库的屏幕上
* 简单便捷

### （3）image2lcd(图片取模软件)

![](image/14.png)

* 图片取模生成C语言数组
* 可实现简单调整

### （4）NetAssist（网络调试助手）

![](image/15.png)

* TCP客户端、服务端
* 单文件体积小、使用编辑

### （5）Proteus（单片机仿真软件）

![](image/16.png)

* 支持89C51
* 支持一些场景传感器
* 方便基础部分程序验证

### （6）LTspice(模数电仿真)

![](image/17.png)

* 模数电部分仿真
* 方便波形以及电源的理解
* 适用于模拟电路部分

### （7）WAV文件分析

WAV音频数组提取器【单片机音频处理】https://blog.csdn.net/weixin_42193239/article/details/121288323?spm=1001.2014.3001.5501

![](image/18.png)



* PCM文件到wav文件

![](image/19.png)

* WAV文件转C语言数组

## 四、好用网站

### （1）正点原子

* http://www.openedv.com/forum-2-1.html
* stm32相关程序和硬件知识
* 常用传感器驱动
* 作为搜索引擎使用

### （2）51黑

* [51黑电子论坛-单片机 电子制作DIY MCU 嵌入式技术学习 (51hei.com)](http://www.51hei.com/bbs/)
* stm32和51单片机程序
* 大量传感器程序

### （3）电路城

* [电子电路图_电路原理图_电子资料下载_电路城 - 与非网 (eefocus.com)](https://www.eefocus.com/circuit/)
* 开源或收费原理图PCB设计

### （4）立创开源广场

* [开源广场-嘉立创EDA开源硬件平台，硬件工程师的电路家园。 (oshwhub.com)](https://oshwhub.com/explore)
* 寻找大神最新设计
* 硬件知识和开源电路

### （5）TCP在线调试

* [wstool (luatos.com)](https://netlab.luatos.com/)

* 实现TCP服务器端

### （6）datasheet查询（手册查询网站）

* https://www.datasheet5.com/

### （7）立创商城

* [立创商城_电子元器件采购网上商城_致力于领先的现货元器件交易平台-嘉立创电子商城 (szlcsc.com)](https://www.szlcsc.com/?c=SO&qhclickid=d8efcc2fed7d2748)
* 芯片手册查询

### （8）ChatGpt(所有需求)

* 自行寻找方式使用
* 小到流水灯大到系统驱动均可查询
* 硬件软件都支持